import java.io.*; 
import java.net.*;
import java.security.*;

class Cabinet
{



public Cabinet(long id, Socket s, DataInputStream is, DataOutputStream os)
{
cabID=id;
tcpConn=s;
sIn=is;
sOut=os;
connected=true;
}

long cabID;
boolean connected;
Socket tcpConn;
DataInputStream sIn;
DataOutputStream sOut;

private static int performTest;

public long getID() {return(cabID);}
public boolean isConnected() {return(connected);}

public void cabinetUpdate(Socket s, DataInputStream is, DataOutputStream os)
{
if(connected) 
	{
	System.out.println("CLOSET " + cabID + " WAS ALREADY CONNECTED (DUPLICATE ID ?)");
	try { s.close(); } catch(IOException ec) { return;} 
	return;
	}
tcpConn=s;
sIn=is;
sOut=os;
connected=true;
}


public static void testClear() {performTest=0;}
public static void testBadHash() {performTest=1;}
public static void testReadTimeout() {performTest=2;}
public static void testVersion() {performTest=3;}
public static void testSendDump() {performTest=4;}
public static void testClockDrift() {performTest=5;}
public static void testKeepAliveStress() {performTest=6;}

public static boolean testKeepAliveStressActive() {return(performTest==6);}

public static String getCurrentTest()
{
String not1=" (not applied to 1st cabinet message)";
switch(performTest)
	{
	case 1: return("BAD HASH" + not1);
	case 2: return("READ TIMEOUT" + not1);
	case 3: return("VERSION" + not1);
	case 4: return("SEND DUMP" + not1);
	case 5: return("CLOCK DRIFT" + not1);
	case 6: return("KEEP ALIVE STRESS (starting after next keep alive)");
	}
return("NONE");
}


public void sendNOP()
{
byte[] data = new byte[500];

if(!connected) return;

sendRequest(ProtocolData.HASH_ID_SHA1, ProtocolData.MSG_ID_NOP, data, 0);
if(receiveReply(data,100))
	{
	if(data[11]==ProtocolData.MSG_ID_NOP_REPLY)
		if(performTest!=6) System.out.println("Valid NOP reply received from cabinet " + cabID);
	}
}

public void sendConfig()
{
byte[] data = new byte[500];
int i,p;

if(!connected) return;

sendRequest(ProtocolData.HASH_ID_SHA1, ProtocolData.MSG_ID_CONFIG, data, 0);
if(receiveReply(data,500))
	{
	if(data[11]==ProtocolData.MSG_ID_CONFIG_REPLY)
		System.out.println("Valid CONFIG reply received from cabinet " + cabID);

	System.out.println(" - Cabinet has " + data[12] + " shelfs");
	p=13;
	for(i=0;i<data[12];i++)
		{
		System.out.print("Shelf " + (i+1) + " : " + data[p] + "x" + data[p+1] + "x" + data[p+2]);
		System.out.print(" - " + data[p+3]);
		if(data[p+4]!=0) System.out.println(" - REFRIG."); else System.out.println(" - NOT REFRIG.");
		p=p+5;
		}
		
	}
}

public void sendStatus()
{
byte[] data = new byte[500];
int i,p;
long temp, tbyte;

if(!connected) return;

sendRequest(ProtocolData.HASH_ID_SHA1, ProtocolData.MSG_ID_STATUS, data, 0);
if(receiveReply(data,500))
        {
	if(performTest==6) return;
        if(data[11]==ProtocolData.MSG_ID_STATUS_REPLY)
                System.out.println("Valid STATUS reply received from cabinet " + cabID);

        System.out.println(" - Cabinet has " + data[12] + " shelfs");
        p=13;
        for(i=0;i<data[12];i++)
                {
                System.out.print("Shelf " + (i+1));
		switch(data[p]) {
                	case 0: System.out.print(" CLOSED"); break;
                	case 1: System.out.print("  OPEN "); break;
                	case 2: System.out.print(" ACCESS"); break;
                	default: System.out.print("   ??? ");
                	}
		if(data[p+1]==1) System.out.print(" REFR.ON  ");
		else System.out.print(" REFR.OFF ");
		if(data[p+2]>=0) tbyte=(long)data[p+2]; else tbyte=256+(long)data[p+2];
                temp=256*tbyte;
                if(data[p+3]>=0) tbyte=(long)data[p+3]; else tbyte=256+(long)data[p+3];
                temp=temp+tbyte;
                System.out.println(temp + " kelvin");
                p=p+4;
                }

        }
}


public void sendStatusChange(byte request, byte shelf)
{
byte[] data = new byte[500];
int i,p;
long temp, tbyte;
data[0]=request;
data[1]=shelf;
sendRequest(ProtocolData.HASH_ID_SHA1, ProtocolData.MSG_ID_REQUEST, data, 2);
if(receiveReply(data,500))
        {
        if(data[11]==ProtocolData.MSG_ID_STATUS_REPLY)
                System.out.println("Valid STATUS reply received from cabinet " + cabID);

        System.out.println(" - Cabinet has " + data[12] + " shelfs");
        p=13;
        for(i=0;i<data[12];i++)
                {
                System.out.print("Shelf " + (i+1));
                switch(data[p]) {
                        case 0: System.out.print(" CLOSED"); break;
                        case 1: System.out.print("  OPEN "); break;
                        case 2: System.out.print(" ACCESS"); break;
                        default: System.out.print("   ??? ");
                        }
                if(data[p+1]==1) System.out.print(" REFR.ON  ");
                else System.out.print(" REFR.OFF ");
		if(data[p+2]>=0) tbyte=(long)data[p+2]; else tbyte=256+(long)data[p+2];
                temp=256*tbyte;
                if(data[p+3]>=0) tbyte=(long)data[p+3]; else tbyte=256+(long)data[p+3];
                temp=temp+tbyte;
                System.out.println(temp + " kelvin");
                p=p+4;
                }

        }
}



private boolean receiveReply(byte data[], int dataMaxLen)
{
long nowTime, msgTime, diffTime;
long cabinetID;
MessageDigest md;
byte[] rem_d = new byte[64];
byte[] my_d = new byte[64];
int p,i,hashSize=0,dataLen;
String MDname;

try { // read header
	for(i=0;i<12;i++) data[i]=(byte)sIn.read(); }
catch(IOException ex)  { connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}
if(data[0]!=ProtocolData.MAJ_VER || data[1]!=ProtocolData.MIN_VER)
	{ connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}
// TIMESTAMP - SECONDS
nowTime=System.currentTimeMillis()/1000;
msgTime=ProtocolData.bytes2ulong(data,2);
if(nowTime>msgTime) diffTime=nowTime-msgTime; else diffTime=msgTime-nowTime;
if(diffTime>ProtocolData.maxClockDrift)
	{ connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}
cabinetID=ProtocolData.bytes2ulong(data,6);
if(cabID!=cabinetID)
	{ connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}


if(data[10]==ProtocolData.HASH_ID_MD5) {MDname="MD5";hashSize=16;} // HASH ID
else
if(data[10]==ProtocolData.HASH_ID_SHA1) {MDname="SHA1";hashSize=20;}
else
	{ connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}

try { md = MessageDigest.getInstance(MDname);}
catch(NoSuchAlgorithmException ex) { connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}


if(data[11]==ProtocolData.MSG_ID_NOP_REPLY) { // MSG ID
		dataLen=0;
	}
else
if(data[11]==ProtocolData.MSG_ID_CONFIG_REPLY) { // MSG ID
    try {
	data[12]=(byte)sIn.read();
	dataLen=1+5*data[12];
	p=13;
	for(i=0;i<5*data[12];i++) {data[p]=(byte)sIn.read(); p++; }
	}
     catch(IOException ex)  { connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}
	}
else
if(data[11]==ProtocolData.MSG_ID_STATUS_REPLY) { // MSG ID
    try {
	data[12]=(byte)sIn.read();
	dataLen=1+4*data[12];
	p=13;
	for(i=0;i<4*data[12];i++) {data[p]=(byte)sIn.read(); p++; }
	}
     catch(IOException ex)  { connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}
	}

else
	{ connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}

md.reset();
md.update(data,0,12);
if(dataLen>0) md.update(data,12,dataLen);
md.update(ProtocolData.secret,0,512);
try { md.digest(my_d,0,hashSize);}
catch(DigestException ex) { connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}


try { // read remote digest
	for(i=0;i<hashSize;i++) rem_d[i]=(byte)sIn.read(); }
catch(IOException ex)  { connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}

for(i=0;i<hashSize;i++)
	if(rem_d[i]!=my_d[i])
		{ connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}

return(true);
}


private boolean sendRequest(byte hashId, byte msgId, byte data[], int dataLen)
{
byte[] h = new byte[12];
byte[] d = new byte[64];
long nowTime;
MessageDigest md;
int i, hashSize=0;
String MDname;

h[0]=ProtocolData.MAJ_VER;
if(performTest==3) h[0]=0;
h[1]=ProtocolData.MIN_VER;
nowTime=System.currentTimeMillis()/1000;
if(performTest==5) nowTime=nowTime-200;
ProtocolData.long2bytes(nowTime, h, 2);
ProtocolData.long2bytes(cabID,h,6);
h[10]=hashId;
h[11]=msgId;

try { sOut.write(h,0,12); }
catch(IOException ex)  { connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}

if(dataLen>0) {
try { sOut.write(data,0,dataLen); }
catch(IOException ex)  { connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}
}


if(hashId==ProtocolData.HASH_ID_MD5) { MDname="MD5"; hashSize=16;}
else
if(hashId==ProtocolData.HASH_ID_SHA1) { MDname="SHA1"; hashSize=20;}
else
	{ connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}

try { md = MessageDigest.getInstance(MDname);}
	catch(NoSuchAlgorithmException ex) { connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}

md.reset();
md.update(h,0,12);
if(dataLen>0) md.update(data,0,dataLen);
md.update(ProtocolData.secret,0,512);
try { md.digest(d,0,hashSize);}
catch(DigestException ex) { connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}
if(performTest==1) d[5]=0;
if(performTest==2) hashSize=10;
try { sOut.write(d,0,hashSize); }
catch(IOException ex)  { connected=false; try { tcpConn.close(); } catch(IOException ec) { return(false);} return(false);}

if(performTest==4) {
	for(i=0;i<200;i++)
		try { sOut.write(d,0,hashSize); }
		catch(IOException ex)  { return(false);}
	}

return(true);
}




} // END CLASS
