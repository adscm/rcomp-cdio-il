import java.io.*; 
import java.net.*;
import java.util.Random;
import java.security.*;
import java.util.concurrent.*;
import java.util.Scanner;

class CabinetsManagement
{

public static Cabinet[] cabList= new Cabinet[50];
public static int numCabs=0;
public static Semaphore cabChange = new Semaphore(1);



public static void main(String args[])
{
int i,op,cnum=0,snum=1;
Scanner in = new Scanner(System.in);
Thread UDPlisten = new Thread(new ListenCabinets(ProtocolData.PORT_NUMBER));
Thread keepAlive = new Thread(new KeepAliveCabinets());

if(!ProtocolData.readSecret("secret.bin")) { System.out.println("Failed to read secret file"); System.exit(1); }



UDPlisten.start();
keepAlive.start();

Cabinet.testClear();

while(true)
	{

	System.out.println("\n\nAVAILABLE OPTIONS:");
	System.out.println("------------------");
	System.out.println("1 - List cabinets");
	System.out.println("------------------");
	System.out.println("10 - CLEAR TESTS      Current test: " + Cabinet.getCurrentTest());
	System.out.println("11 - TEST BAD HASH");
	System.out.println("12 - TEST READ TIMEOUT");
	System.out.println("13 - TEST VERSION");
	System.out.println("14 - TEST SEND DUMP");
	System.out.println("15 - TEST CLOCK DRIFT");
	System.out.println("16 - TEST KEEP ALIVE STRESS");
	System.out.println("------------------");
	System.out.println("20 - SEND NOP");
	System.out.println("21 - SEND CONFIG");
	System.out.println("22 - SEND STATUS");
	System.out.println("------------------");
	System.out.println("50 - OPEN SHELF");
	System.out.println("51 - CLOSE SHELF");
	System.out.println("52 - ACCESS SHELF");
	System.out.println("------------------");
	op=in.nextInt();
	if(op>19) {
		System.out.print("Enter cabinet number: ");
		cnum=in.nextInt(); cnum=cnum-1;
		}
	if(op>49) {
		System.out.print("Enter shelf number: ");
		snum=in.nextInt();
		}
	switch(op) {

	case 1:
	System.out.println("\n\n\n\n\n\r------------------ CABINETS LIST -----------------------");
	try { cabChange.acquire();} catch(InterruptedException ex) { System.out.println("Lock interrupted"); }
	for(i=0;i<numCabs;i++)
		{
		System.out.print("Cabinet " + (i+1) + " ID: " + cabList[i].getID());
		if(cabList[i].isConnected()) System.out.println(" connected");
		else System.out.println(" NOT connected");
		}
	cabChange.release();
	System.out.println("--------------------------------------------------------\n\n");
	break;


	case 10:Cabinet.testClear(); break;
	case 11:Cabinet.testBadHash(); break;
	case 12:Cabinet.testReadTimeout(); break;
	case 13:Cabinet.testVersion(); break;
	case 14:Cabinet.testSendDump(); break;
	case 15:Cabinet.testClockDrift(); break;
	case 16:Cabinet.testKeepAliveStress(); break;

	case 20:
	try { cabChange.acquire();} catch(InterruptedException ex) { System.out.println("Lock interrupted"); }
	if(cnum>=numCabs) System.out.println("Invalid cabinet number");
	else cabList[cnum].sendNOP();
	cabChange.release();
	break;

	case 21:
	try { cabChange.acquire();} catch(InterruptedException ex) { System.out.println("Lock interrupted"); }
	if(cnum>=numCabs) System.out.println("Invalid cabinet number");
	else cabList[cnum].sendConfig();
	cabChange.release();
	break;

	case 22:
	try { cabChange.acquire();} catch(InterruptedException ex) { System.out.println("Lock interrupted"); }
	if(cnum>=numCabs) System.out.println("Invalid cabinet number");
	else cabList[cnum].sendStatus();
	cabChange.release();
	break;

	case 50:
	try { cabChange.acquire();} catch(InterruptedException ex) { System.out.println("Lock interrupted"); }
	if(cnum>=numCabs) System.out.println("Invalid cabinet number");
	else cabList[cnum].sendStatusChange((byte)snum,(byte)0);
	cabChange.release();
	break;

	case 51:
	try { cabChange.acquire();} catch(InterruptedException ex) { System.out.println("Lock interrupted"); }
	if(cnum>=numCabs) System.out.println("Invalid cabinet number");
	else cabList[cnum].sendStatusChange((byte)snum,(byte)1);
	cabChange.release();
	break;

	case 52:
	try { cabChange.acquire();} catch(InterruptedException ex) { System.out.println("Lock interrupted"); }
	if(cnum>=numCabs) System.out.println("Invalid cabinet number");
	else cabList[cnum].sendStatusChange((byte)snum,(byte)2);
	cabChange.release();
	break;


	default:
		System.out.println("\nInvalid option\n");


	}


	}




//try { UDPlisten.join(); }
//catch(InterruptedException ex) { System.out.println("Join interrupted"); }

}



public static void addCabinet(InetAddress ip) // JUST TESTING
{
int i;
Socket tcpConn;
DataInputStream sIn;
DataOutputStream sOut;
byte[] data = new byte[500];
byte[] digest = new byte[128];
long nowTime, msgTime, diffTime;
long cabinetID;
MessageDigest reqMD, repMD;
String MDname;
int hashSize;


try { 	tcpConn = new Socket(ip,ProtocolData.PORT_NUMBER);
	tcpConn.setSoTimeout(5000);
	} catch(IOException ex) {return;}

try {
	sIn = new DataInputStream(tcpConn.getInputStream());
	sOut = new DataOutputStream(tcpConn.getOutputStream());
	} catch(IOException ex) { try { tcpConn.close(); } catch(IOException ec) { return;} return;}



// PROTO VERSION
data[0]=ProtocolData.MAJ_VER; data[1]=ProtocolData.MIN_VER;
// TIMESTAMP - SECONDS
nowTime=System.currentTimeMillis()/1000;
ProtocolData.long2bytes(nowTime, data, 2);

// CABINET ID - ZERO FOR FIRST REQUEST TO UNKNOWN CABINET
data[6]=data[7]=data[8]=data[9]=0;

// HASH function and request type
//data[10]=ProtocolData.HASH_ID_MD5;
data[10]=ProtocolData.HASH_ID_SHA1;

// REQUEST CODE
data[11]=ProtocolData.MSG_ID_NOP;
try { sOut.write(data,0,12); }
catch(IOException ex)  { try { tcpConn.close(); } catch(IOException ec) { return;} return;}


// SEND A BAD MAC - JUST TESTING
// try { sOut.write(data,12,16); }
// catch(IOException ex)  { try { tcpConn.close(); } catch(IOException ec) { return;} return;}


// CALCULATE THE CORRECT MAC

if(data[10]==ProtocolData.HASH_ID_MD5) {MDname="MD5";hashSize=16;} // HASH ID
else
if(data[10]==ProtocolData.HASH_ID_SHA1) {MDname="SHA1";hashSize=20;}
else
	{ try { tcpConn.close(); } catch(IOException ec) { return;} return;}

try { reqMD = MessageDigest.getInstance(MDname);}
catch(NoSuchAlgorithmException ex) { try { tcpConn.close(); } catch(IOException ec) { return;} return;}


reqMD.update(data,0,12);
reqMD.update(ProtocolData.secret,0,512);
try { reqMD.digest(data,12,hashSize);}
catch(DigestException ex) { try { tcpConn.close(); } catch(IOException ec) { return;} return;}

try { sOut.write(data,12,hashSize); }
catch(IOException ex)  { try { tcpConn.close(); } catch(IOException ec) { return;} return;}

//System.out.println("NOP sent");
// System.out.println("SENT NOP REQUEST, SEQ.NUM. " + ProtocolData.bytes2ulong(data,2));

// READ THE REPLY (NOP_REPLY - 12 bytes)
try {
	for(i=0;i<12;i++) data[i]=(byte)sIn.read(); }
catch(IOException ex)  { try { tcpConn.close(); } catch(IOException ec) { return;} return;}

if(data[0]!=ProtocolData.MAJ_VER || data[1]!=ProtocolData.MIN_VER)
	{ try { tcpConn.close(); } catch(IOException ec) { return;} return;}

// TIMESTAMP - SECONDS
nowTime=System.currentTimeMillis()/1000;
msgTime=ProtocolData.bytes2ulong(data,2);

if(nowTime>msgTime) diffTime=nowTime-msgTime; else diffTime=msgTime-nowTime;
// System.out.println("New cabinet, clock drift = " + diffTime);

if(diffTime>ProtocolData.maxClockDrift) { try { tcpConn.close(); } catch(IOException ec) { return;} return;}

cabinetID=ProtocolData.bytes2ulong(data,6);

if(data[11]!=ProtocolData.MSG_ID_NOP_REPLY)
        { System.out.println("BAD CODE"); try { tcpConn.close(); } catch(IOException ec) { return;} return;}

// System.out.println("Getting NOP reply, difftime: " + diffTime);

// CALCULATE DIGEST OF REPLY
if(data[10]==ProtocolData.HASH_ID_MD5) {MDname="MD5";hashSize=16;} // HASH ID
else
if(data[10]==ProtocolData.HASH_ID_SHA1) {MDname="SHA1";hashSize=20;}
else
        { try { tcpConn.close(); } catch(IOException ec) { return;} return;}

try { repMD = MessageDigest.getInstance(MDname);}
catch(NoSuchAlgorithmException ex) { try { tcpConn.close(); } catch(IOException ec) { return;} return;}

try {
	for(i=12;i<12+hashSize;i++) data[i]=(byte)sIn.read(); }
catch(IOException ex)  { try { tcpConn.close(); } catch(IOException ec) { return;} return;}

repMD.update(data,0,12);
repMD.update(ProtocolData.secret,0,512);

try { repMD.digest(digest,0,hashSize);}
catch(DigestException ex) { try { tcpConn.close(); } catch(IOException ec) { return;} return;}

// CHECK IF DIGEST OK
for(i=0;i<hashSize;i++)
	if(digest[i]!=data[12+i]) { System.out.println("Failed DIGEST");
		try { tcpConn.close(); } catch(IOException ec) { return;} return; }

// System.out.println("DIGEST OK, SEQ.NUM. " + newSeqNum);
// System.out.println("CABINET ID: " + cabinetID);

try { cabChange.acquire();} catch(InterruptedException ex) { System.out.println("Lock interrupted"); }
for(i=0;i<numCabs;i++) 
	{
	if(cabinetID==cabList[i].getID()) break;
	}

if(i==numCabs) {
		cabList[i]= new Cabinet(cabinetID, tcpConn, sIn, sOut);
		numCabs++; 
		}
else {
	cabList[i].cabinetUpdate(tcpConn, sIn, sOut);}
cabChange.release();
	
}




} //END CLASS
