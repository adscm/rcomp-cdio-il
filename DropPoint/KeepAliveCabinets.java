import java.io.*; 
import java.net.*;

class KeepAliveCabinets implements Runnable 
{

private static boolean stop;

public KeepAliveCabinets() {stop=false;}

public void run()
{
int cab;
while(!stop)                
	{
	try { 
		if(Cabinet.testKeepAliveStressActive()) Thread.sleep(50);
		else Thread.sleep(1000*ProtocolData.keepAliveFrequency);
		}
        catch(InterruptedException ex) 
		{ System.out.println("Sleep interrupted"); }

	try { CabinetsManagement.cabChange.acquire();} catch(InterruptedException ex) 
		{ System.out.println("Lock interrupted"); return;}

	for(cab=0;cab<CabinetsManagement.numCabs;cab++) CabinetsManagement.cabList[cab].sendNOP();
	CabinetsManagement.cabChange.release();
	}



}

static public void stopThread() { stop=true;}

} // END CLASS
