import java.io.*; 
import java.net.*;

class ProtocolData
{
public static int PORT_NUMBER=9002;

public static byte MAJ_VER=1;
public static byte MIN_VER=1;

public static byte HASH_ID_MD5=1;
public static byte HASH_ID_SHA1=2;


public static byte MSG_ID_NOP=100;
public static byte MSG_ID_NOP_REPLY=101;

public static byte MSG_ID_CONFIG=10;
public static byte MSG_ID_CONFIG_REPLY=11;

public static byte MSG_ID_STATUS=12;
public static byte MSG_ID_STATUS_REPLY=13;

public static byte MSG_ID_REQUEST=1;

public static byte[] secret=new byte[512];

public static int maxClockDrift=60;

public static int keepAliveFrequency=60;


public static boolean readSecret(String fileName)
{
File f;
InputStream fReader;

f=new File(fileName);
if(!f.exists()) return(false);
try { fReader = new BufferedInputStream(new FileInputStream(f));} 
catch(FileNotFoundException ex) { return(false);}
try { fReader.read(secret,0,512); fReader.close(); }
catch(IOException ex) { return(false);}
return(true);
}


public static long bytes2ulong(byte[] data, int off)
{
long fact=1;
long dU=0;
long v=0;
int i;
for(i=3;i>=0;i--) {
        if(data[off+i]>=0) dU=(long)data[off+i]; else dU=256+(long)data[off+i];
        v=v+fact*dU; fact=fact*256;}
return(v);
}

public static void long2bytes(long v, byte[] data, int off)
{
int i;
for(i=3;i>=0;i--) { data[off+i]=(byte)(v%256); v=v/256;}
}


} // END CLASS
