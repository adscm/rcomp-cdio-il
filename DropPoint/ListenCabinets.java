import java.io.*; 
import java.net.*;

class ListenCabinets implements Runnable 
{
int listenPort;

static DatagramSocket sock;
static private boolean stop;

public ListenCabinets(int port) { listenPort=port; stop=false;}

public void run()
{
InetAddress cabIP;
byte[] data=new byte[300];


try { sock = new DatagramSocket(listenPort); }
catch(BindException ex)
                {
                System.out.println("UDP port is busy.");
                System.exit(1);
                }
catch(SocketException ex)
                {
                System.out.println("Failed to create UDP socket.");
                System.exit(1);
                }

DatagramPacket ann = new DatagramPacket(data, 2);

while(!stop)                
	{

	try { sock.receive(ann);}
	catch(IOException ex) { System.out.println("Failed to receive UDP datagram");}
	if(ann.getLength()==2)
		{
		if(data[0]==ProtocolData.MAJ_VER && data[1]==ProtocolData.MIN_VER)
			{
			cabIP=ann.getAddress();
			// System.out.println("Cabinet announcement received from: " + cabIP.getHostAddress());
			CabinetsManagement.addCabinet(cabIP);
			}
		}


	}

sock.close();

}

static public void stopThread() { stop=true;}

} // END CLASS
