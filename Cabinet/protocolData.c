#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#include <openssl/md5.h>
#include <openssl/sha.h>

#include "protocolData.h"
#include "physics.h"

unsigned char protoVersion=DEF_PROTOCOL_VERSION;
unsigned char protoSubVersion=DEF_PROTOCOL_SUB_VERSION;
unsigned int portNumber=DEF_PORT_NUMBER;
unsigned int broadcastFrequency=DEF_BROADCAST_FREQUENCY;
unsigned int readTimeout=DEF_READ_TIMEOUT;
unsigned int keepAliveTimeout=DEF_KEEP_ALIVE_TIMEOUT;
unsigned int maxClockDrift=DEF_MAX_CLOCK_DRIFT;
char *broadcastAddress=DEF_BROADCAST_ADDRESS;

unsigned char DPsecret[512];
unsigned char cabinetID[4];


int readSecret(char *secFile);

int readConfig(char *cfgFile)
{
char gotID=0, gotSecret=0, gotPhysics=0;
int i;
char *aux, line[200];
FILE *f;
unsigned int id;

f=fopen(cfgFile,"r");
if(!f) return(0);


while(fgets(line,200,f))
	{
	if(!strncmp(line,"ID ",3))
		{
		aux=line+3; while(*aux<=32) aux++;
		sscanf(aux,"%u\n",&id);
		for(i=3;i>=0;i--) { cabinetID[i]=id%256; id=id/256;}
		printf("Got from config: Cabinet ID: %lu\n",bytes2long(cabinetID));
		gotID=1;
		}
	else
	if(!strncmp(line,"SECRET ",7))
		{
		line[strlen(line)-1]=0;
		aux=line+7; while(*aux<=32) aux++;
		if(readSecret(aux)) gotSecret=1;
		}
	else
	if(!strncmp(line,"BROADCAST ADDRESS ",18))
		{
		line[strlen(line)-1]=0;
		aux=line+18; while(*aux<=32) aux++;
		broadcastAddress=strdup(aux);
		printf("Got from config: BROADCAST ADDRESS: %s\n",broadcastAddress);
		}
	else
	if(!strncmp(line,"PHYSICS ",8))
		{
		line[strlen(line)-1]=0;
		aux=line+8; while(*aux<=32) aux++;
		if(readPhysics(aux)) gotPhysics=1;
		}
	else
	if(!getConfigByte(line,"PROTO VERSION ",&protoVersion))
	if(!getConfigByte(line,"PROTO SUB VERSION ",&protoSubVersion))
	if(!getConfigInt(line,"PORT NUMBER ",&portNumber))
	if(!getConfigInt(line,"BROADCAST FREQUENCY ",&broadcastFrequency))
	if(!getConfigInt(line,"MAX CLOCK DRIFT ",&maxClockDrift))
	if(!getConfigInt(line,"READ TIMEOUT ",&readTimeout))
	if(!getConfigInt(line,"KEEP ALIVE TIMEOUT ",&keepAliveTimeout))
	if(*line!='/' && *line!='#' && *line>32)
		printf("Unknown config tag: %s",line);



	}
fclose(f);
if(!gotID) { puts("Failed to get cabinet ID"); return(0);}
if(!gotSecret) { puts("Failed to get shared secret"); return(0);}
if(!gotPhysics) { puts("Failed to get cabinet physics"); return(0);}
return(1);
}

char getConfigByte(char *line, char *tag, unsigned char *val)
{
int tlen=strlen(tag);
char *aux;
if(strncmp(line,tag,tlen)) return(0);
line[strlen(line)-1]=0;
aux=line+tlen; while(*aux<=32) aux++;
*val=(unsigned char) atoi(aux);
printf("Got from config: %s%i\n",tag,*val);
return(1);
}

char getConfigInt(char *line, char *tag, unsigned int *val)
{
int tlen=strlen(tag);
char *aux;
if(strncmp(line,tag,tlen)) return(0);
line[strlen(line)-1]=0;
aux=line+tlen; while(*aux<=32) aux++;
*val=(unsigned int) atoi(aux);
printf("Got from config: %s%i\n",tag,*val);
return(1);
}

int readSecret(char *secFile)
{
FILE *f;
int c;

f=fopen(secFile,"r");
if(!f) return(0);
c=fread(DPsecret,1,512,f);
fclose(f);
if(c!=512) c=0;
return(c);
}


unsigned long bytes2long(unsigned char *data)
{
unsigned long fac=1, res=0;
int i;
for(i=3;i>=0;i--) { res=res+fac*data[i]; fac=256*fac;}
return(res);
}

void long2bytes(unsigned long val, unsigned char *data)
{
int i;
for(i=3;i>=0;i--) { data[i]=val%256; val=val/256;}
}

unsigned char *getCabinetID(void) {return(cabinetID);}
int getPort(void) {return(portNumber);}
int getBroadcastFrequency(void) {return(broadcastFrequency);}
char *getBroadcastAddress(void) {return(broadcastAddress);}


char checkVersion(unsigned char *version) {return(version[0]==protoVersion && version[1]==protoSubVersion);}
void setVersion(unsigned char *version) {version[0]=protoVersion; version[1]=protoSubVersion;}

char checkTimestamp(unsigned char *timestamp) 
{
unsigned long nowTime, msgTime;
long drift;
msgTime=bytes2long(timestamp);
nowTime=time(NULL);
drift=msgTime-nowTime; if(drift<0) drift=-drift;
if(drift>maxClockDrift) return(0);
return(1);
}

char checkCabinetID(unsigned char *cabID) {return(!memcmp(cabID,cabinetID,4));}


int _readExactTimeOut(int s, char *data, int dataLen, int sec)
{
char *aux=data;
int r;
fd_set rs;
struct timeval to;
while(dataLen)
        {
        FD_ZERO(&rs); FD_SET(s,&rs); to.tv_usec=0; to.tv_sec=sec;
        r=select(s+1,&rs,NULL,NULL,&to);
        if(r!=1) return(0);
        if(read(s,aux,1)!=1) return(0);
        aux++; dataLen--;
        }
return(1);
}

int readExactTimeout(int s, char *data, int dataLen) {return(_readExactTimeOut(s,data,dataLen,readTimeout));}
int readExactKeepAliveTimeout(int s, char *data, int dataLen) {return(_readExactTimeOut(s,data,dataLen,keepAliveTimeout));}


unsigned char addDigest(unsigned char *msg, int msgLen)
{
unsigned char *hashID=msg+10;
MD5_CTX md5_c;
SHA_CTX sha1_c;
unsigned char hashLen=0;

if(*hashID==HASH_ID_MD5) {
	MD5_Init(&md5_c); 
	hashLen=16;
	MD5_Update(&md5_c, msg, msgLen);
	MD5_Update(&md5_c, DPsecret, 512);
	MD5_Final(msg+msgLen,&md5_c);
	}
else
if(*hashID==HASH_ID_SHA1) {
	SHA1_Init(&sha1_c); 
	hashLen=20;
	SHA1_Update(&sha1_c, msg, msgLen);
        SHA1_Update(&sha1_c, DPsecret,512);
	SHA1_Final(msg+msgLen, &sha1_c);
	}

return(hashLen);
}


unsigned char checkDigest(unsigned char *msg, int msgLen)
{
unsigned char *hashID=msg+10;
MD5_CTX md5_c;
SHA_CTX sha1_c;
unsigned char digest[128];
unsigned char hashLen=0;

if(*hashID==HASH_ID_MD5) {
	MD5_Init(&md5_c); 
	hashLen=16;
	MD5_Update(&md5_c, msg, msgLen);
	MD5_Update(&md5_c, DPsecret, 512);
	MD5_Final(digest,&md5_c);
	}
else
if(*hashID==HASH_ID_SHA1) {
	SHA1_Init(&sha1_c); 
	hashLen=20;
	SHA1_Update(&sha1_c, msg, msgLen);
        SHA1_Update(&sha1_c, DPsecret,512);
	SHA1_Final(digest, &sha1_c);
	}
if(!hashLen) return(0);
if(memcmp(msg+msgLen,digest,hashLen)) return(0);
return(1);
}

