#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

int DPconnected(int connSock)
{

unsigned char msgR[500];
unsigned char *Mversion=msgR;
unsigned char *MseqNum=Mversion+2;
unsigned char *MclosetID=MseqNum+4;
unsigned char *MhashID=MclosetID+4;
unsigned char *MmsgTypeID=MhashID+1;
unsigned char *Mdata=MmsgTypeID+1;
unsigned char *aux;

unsigned char *Mhash;

char firstMsg=1;
char waitPerformTests=3;
int i, dataLen, hashLen;
unsigned long nowTime;

while(1)
	{

	if(waitPerformTests) waitPerformTests--;

	if(!waitPerformTests && performTest[testUnsolicitedDump]) 
		{
		printTest(testUnsolicitedDump);
		while(1) {sleep(1);write(connSock,msgR,128);}
		}

	if(!waitPerformTests && performTest[testReadTimeout]) { printTest(testReadTimeout);sleep(200);}

	if(!readExactKeepAliveTimeout(connSock,msgR,12)) return(2);
	if(!checkVersion(Mversion)) return(3);
	if(!checkTimestamp(MseqNum)) return(4);
	
	if(firstMsg) 
		{ // FIRST MSG - CLOSET ID SHOULD BE ZERO
		if(bytes2long(MclosetID)!=0) return(5);
		firstMsg=0;
		}
	else
		{ // NOT FIRST MSG - CHECK CLOSET ID
		if(!checkCabinetID(MclosetID)) return(5);
		}

	if(*MmsgTypeID==MSG_ID_NOP || *MmsgTypeID==MSG_ID_CONFIG || *MmsgTypeID==MSG_ID_STATUS) dataLen=0;
	else
	if(*MmsgTypeID==MSG_ID_REQUEST)
		{
		dataLen=2;
		if(!readExactTimeout(connSock,Mdata,dataLen)) return(8);
		}
	// OTHER MSG TYPES
	else return(9);

	// CHECK MESSAGE AUTHENTICATION CODE (MAC)
	Mhash=Mdata+dataLen;
	if(*MhashID==HASH_ID_MD5) hashLen=16;
	else
	if(*MhashID==HASH_ID_SHA1) hashLen=20;
	else return(10);
	if(!readExactTimeout(connSock,Mhash,hashLen)) return(11);
	if(!checkDigest(msgR,12+dataLen)) return(12);

	if(*MmsgTypeID==MSG_ID_REQUEST) {
		execCommand(Mdata[1],Mdata[0]-1); // ORDER, SHELF
		}




	// SEND REPLY
	nowTime=time(NULL);
	if(!waitPerformTests && performTest[testClockDrift]) {printTest(testClockDrift);nowTime-=500;}
	long2bytes(nowTime,MseqNum);
	memcpy(MclosetID,getCabinetID(),4);
	
	if(*MmsgTypeID==MSG_ID_NOP)
		{
		*MmsgTypeID=MSG_ID_NOP_REPLY;
		dataLen=0;
		}
	else
	if(*MmsgTypeID==MSG_ID_CONFIG)
		{
		*MmsgTypeID=MSG_ID_CONFIG_REPLY;
		dataLen=copyConfig2msg(Mdata);
		}
	else
	if(*MmsgTypeID==MSG_ID_STATUS||*MmsgTypeID==MSG_ID_REQUEST)
		{
		*MmsgTypeID=MSG_ID_STATUS_REPLY;
		dataLen=copyStatus2msg(Mdata);
		}
	else return(13);

	if(!waitPerformTests && performTest[testBadVersion]) {printTest(testBadVersion);*Mversion=0;}
	if(!waitPerformTests && performTest[testBadReply]) {printTest(testBadReply);*MmsgTypeID=55;}
	
	if(write(connSock,msgR,12)!=12) return(20); // HEADER
        if(dataLen)
		{
		if(write(connSock,Mdata,dataLen)!=dataLen) return(21);
		}
	Mhash=Mdata+dataLen;
	hashLen=addDigest(msgR,12+dataLen);
	if(!hashLen) return(22);

	if(!waitPerformTests && performTest[testBadHash]) {printTest(testBadHash);Mhash[5]++;}
	if(write(connSock,Mhash,hashLen)!=hashLen) return(23);
	if(!waitPerformTests && performTest[testDumpData])
		{
		printTest(testDumpData);
		while(1) {sleep(1);write(connSock,msgR,hashLen);}
		}

	
	printStatus();
	}

}



