#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>

int main(int argc, char **argv)
{
FILE *f;
long ll;
int i;
unsigned char DPsecret[512];

if(argc!=2) { printf("Usage: %s fileToHoldSecret\n", argv[0]); exit(1);}

srandom(time(NULL));
f=fopen(argv[1],"w");
if(!f) { printf("Failed to open %s for writing\n", argv[1]); exit(1);}
for(i=0;i<512;i++)
	{
	ll=random();
	ll=ll/70000;
	DPsecret[i]=ll%256;
	}
i=fwrite(DPsecret,1,512,f);
if(i!=512) { printf("Failed writing to %s\n", argv[1]); exit(1);}
fclose(f);
exit(0);
}
