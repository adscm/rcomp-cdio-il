

#define NUM_TESTS 9

char *testName[]={"testBadVersion","testBadHash","testDumpData","testBadReply","testReadTimeout","testClockDrift",
		"testBigUDP","testBadUDPversion","testUnsolicitedDump"};

char performTest[NUM_TESTS];


char testBadVersion=0;
char testBadHash=1;
char testDumpData=2;
char testBadReply=3;
char testReadTimeout=4;
char testClockDrift=5;
char testBigUDP=6;
char testBadUDPversion=7;
char testUnsolicitedDump=8;





char reconnect=1;

char getTest(char *line, char *tag, char *val);


void parseArgTests(int nargs, char **tests)
{
int i,j;

for(j=0;j<NUM_TESTS;j++) performTest[j]=0;

printf("\x1B[31mTesting for :");
for(i=1;i<nargs;i++)
	{
	for(j=0;j<NUM_TESTS;j++)
		if(!strcmp(tests[i],testName[j])) 
			{
			performTest[j]=1;
			printf(" %s",testName[j]);
			reconnect=0;
			break;
			}
	if(j==NUM_TESTS)
			{
			printf("\n\x1B[37mUnknown test in command line: %s\nAvailable tests are:",tests[i]);
			for(j=0;j<NUM_TESTS;j++) printf(" %s",testName[j]);
			puts("");
			exit(1);
			}
	}

if(reconnect) puts(" NO TESTS\x1B[37m\n");
else puts("\nTCP TESTS WILL RUN ON 2nd MESSAGE\x1B[37m\n");
}

printTest(int t)
{
printf("\x1B[31mTESTING: %s ...\x1B[37m\n", testName[t]);
}
