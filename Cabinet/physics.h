
#ifndef PHYSICS_H
#define PHYSICS_H

#define MAX_EXTERN_COMMAND_LINE_LENGHT 200

#define IPC_ID 4561


typedef struct {
                unsigned char shelfHeight;
                unsigned char shelfWidth;
                unsigned char shelfDepth;
                unsigned char shelfGroundClearance;
                unsigned char shelfRefrigerationSystem;
                char shelfOpenCmd[MAX_EXTERN_COMMAND_LINE_LENGHT];
                char shelfCloseCmd[MAX_EXTERN_COMMAND_LINE_LENGHT];
                char shelfAccessCmd[MAX_EXTERN_COMMAND_LINE_LENGHT];
                char shelfReadStatusCmd[MAX_EXTERN_COMMAND_LINE_LENGHT];
                char shelfReadTempereatureCmd[MAX_EXTERN_COMMAND_LINE_LENGHT];
                } shelfConfig;



typedef struct {
		char refrigReadTempereatureCmd[MAX_EXTERN_COMMAND_LINE_LENGHT];
		char refrigOnCmd[MAX_EXTERN_COMMAND_LINE_LENGHT];
		char refrigOffCmd[MAX_EXTERN_COMMAND_LINE_LENGHT];
		char refrigStatusCmd[MAX_EXTERN_COMMAND_LINE_LENGHT];
		} refrigConfig;



typedef struct {
        unsigned char shelfStatus;
        unsigned char shelfRefrigerationOn;
        unsigned int shelfTemperatureKelvin;
        } shelfStatus;


void printStatus(void);
int readPhysics(char *cfgFile);
void execCommand(unsigned char ord, unsigned char shelf);

int copyStatus2msg(unsigned char *place);
int copyConfig2msg(unsigned char *place);

#endif /* PHYSICS_H */
