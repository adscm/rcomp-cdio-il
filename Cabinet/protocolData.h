
#ifndef PROTOCOL_DATA_H
#define PROTOCOL_DATA_H

// PROTOCOL DEFAULTS

#define DEF_PROTOCOL_VERSION 1
#define DEF_PROTOCOL_SUB_VERSION 1

#define DEF_PORT_NUMBER 9002
#define DEF_BROADCAST_FREQUENCY 30

#define DEF_READ_TIMEOUT 5
#define DEF_KEEP_ALIVE_TIMEOUT 120
#define DEF_MAX_CLOCK_DRIFT 5

#define DEF_BROADCAST_ADDRESS "255.255.255.255"

// PROTOCOL DEFINITIONS

#define HASH_ID_MD5 1
#define HASH_ID_SHA1 2
#define HASH_ID_SHA256 3

#define MSG_ID_NOP 100
#define MSG_ID_NOP_REPLY 101

#define MSG_ID_CONFIG 10
#define MSG_ID_CONFIG_REPLY 11

#define MSG_ID_STATUS 12
#define MSG_ID_STATUS_REPLY 13

#define MSG_ID_REQUEST 1

int readConfig(char *cfgFile);

char getConfigByte(char *line, char *tag, unsigned char *val);
char getConfigInt(char *line, char *tag, unsigned int *val);

unsigned long bytes2long(unsigned char *data);

void long2bytes(unsigned long val, unsigned char *data);



unsigned char *getCabinetID(void);
int getPort(void);
int getBroadcastFrequency(void);
char *getBroadcastAddress(void);
char checkVersion(unsigned char *version);
void setVersion(unsigned char *version);
char checkTimestamp(unsigned char *timestamp);
char checkCabinetID(unsigned char *cabID);

unsigned char addDigest(unsigned char *msg, int msgLen);
unsigned char checkDigest(unsigned char *msg, int msgLen);

int readExactTimeout(int s, char *data, int dataLen);
int readExactKeepAliveTimeout(int s, char *data, int dataLen);

#endif /* PROTOCOL_DATA_H */
