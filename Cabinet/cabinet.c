#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#include "tests.c"
#include "protocolData.h"
#include "DPconnected.c"


int main(int argc, char **argv)
{
struct sockaddr_in local, bcast;
int sockUDP, sockTCP, connTCP, adl, res;
int i;
char linha[512];
fd_set readSocks;
struct timeval to;

adl=sizeof(local);

bzero((char *)&bcast,adl);
bcast.sin_family=AF_INET;
bcast.sin_addr.s_addr=inet_addr(getBroadcastAddress());
bcast.sin_port=htons(getPort());

if(!readConfig("cabinet.conf"))
	{
	puts("Failed to read configuration");
	exit(1);
	}

parseArgTests(argc, argv);

while(1)
	{
	puts("(re)starting ...");

        sockUDP=socket(AF_INET,SOCK_DGRAM,0);
	bzero((char *)&local,adl);
	local.sin_family=AF_INET;
	local.sin_addr.s_addr=htonl(INADDR_ANY); /* endereco IP local */
	local.sin_port=htons(0); /* porto local (0=auto assign) */
	bind(sockUDP,(struct sockaddr *)&local,adl);
	i=1; setsockopt(sockUDP,SOL_SOCKET, SO_BROADCAST, &adl, sizeof(i));

        sockTCP=socket(AF_INET,SOCK_STREAM,0);
	i=1; setsockopt(sockTCP,SOL_SOCKET,SO_REUSEADDR,&i,sizeof(i));

	bzero((char *)&local,adl);
	local.sin_family=AF_INET;
	local.sin_addr.s_addr=htonl(INADDR_ANY); /* endereco IP local */
	local.sin_port=htons(getPort());

	if(bind(sockTCP,(struct sockaddr *)&local,adl)== -1)
		{
		perror("Failed to bind TCP socket");
		close(sockTCP); close(sockUDP); exit(1);
		}
	listen(sockTCP,SOMAXCONN);
	do
		{
		puts("Broadcasting");
		setVersion(linha);
		if(performTest[testBadUDPversion]) { printTest(testBadUDPversion); linha[0]=0;}
		if(performTest[testBigUDP]) {
			printTest(testBigUDP);
			sendto(sockUDP,linha,512,0,(struct sockaddr *)&bcast,adl);
			}
		else
			sendto(sockUDP,linha,2,0,(struct sockaddr *)&bcast,adl);
		FD_ZERO(&readSocks); FD_SET(sockTCP, &readSocks);
		to.tv_sec=getBroadcastFrequency(); to.tv_usec=0;
		}
	while(select(sockTCP+1,&readSocks,NULL,NULL,&to)!=1);
	connTCP=accept(sockTCP,(struct sockaddr *)&local,&adl);
	close(sockUDP); close(sockTCP);
	puts("Connected to DropPoint");
	i=DPconnected(connTCP);
	printf("Connection to DropPoint lost, error code: %i\n",i);
	close(connTCP);
	if(!reconnect) exit(1);
	sleep(1);
	}

}
