#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>

#include "physics.h"
#include "protocolData.h"

unsigned char numShelfs=0;
unsigned char numRefrig=0;

unsigned long cabIdLong=0;

shelfConfig *cabinetShelfs=NULL;
refrigConfig *cabinetRefrigs=NULL;

shelfStatus *cabinetStatus=NULL;

int mutexStatus, sharedStatus;

void printStatus(void)
{
int i;
if(!cabIdLong) cabIdLong=bytes2long(getCabinetID());
puts(	"--------------------------------------------------------");
printf("    CABINET ID: %lu\n",cabIdLong);
puts(	"--------------------------------------------------------");
for(i=0;i<numShelfs;i++)
	{
	printf("Shelf %i : ",i+1);
	switch(cabinetStatus[i].shelfStatus) {
		case 0: printf("CLOSED"); break;
		case 1: printf(" OPEN "); break;
		case 2: printf("ACCESS"); break;
		default: printf("  ??? ");
		}
	if(cabinetStatus[i].shelfRefrigerationOn) printf(" REFR.ON  "); else printf(" REFR.OFF ");
	printf(" %i kelvin\n",cabinetStatus[i].shelfTemperatureKelvin);
	}
puts(	"--------------------------------------------------------");
} 

void lockMutex(void) {
struct sembuf op;
op.sem_num=0; op.sem_op=-1; op.sem_flg=SEM_UNDO;
if(semop(mutexStatus,&op,1)==-1) perror("Mutex lock");
}

void unlockMutex(void) {
struct sembuf op;
op.sem_num=0; op.sem_op=1; op.sem_flg=SEM_UNDO;
if(semop(mutexStatus,&op,1)==-1) perror("Mutex unlock");
}


char *getNextField(char *line, char *field)
{
char *aux, *aux1;
aux=line; while(*aux && *aux!='(') aux++; if(!*aux) return(NULL);
aux++; aux1=aux; while(*aux1 && *aux1!=')') aux1++; if(!*aux1) return(NULL);
*aux1=0; strcpy(field,aux);
return(aux1+1);
}



int readPhysics(char *physicsFile)   // TODO - READ CABINET PHYSICS FROM FILE
{
FILE *f;
char *aux, line[200], tmp[50];
unsigned char bb, gotShelfs, gotRefrig;
struct shmid_ds b;


numShelfs=numRefrig=0;
cabinetRefrigs=(refrigConfig *)NULL;
cabinetShelfs=(shelfConfig *)NULL;
sharedStatus=mutexStatus=-1;

f=fopen(physicsFile,"r");
if(!f) return(0);
while(fgets(line,200,f))
        {
        if(!strncmp(line,"S ",2)) numShelfs++;
	else if(!strncmp(line,"R ",2)) numRefrig++;
	}

if(numShelfs) cabinetShelfs=malloc(numShelfs*sizeof(shelfConfig));
if(!cabinetShelfs) return(0);
if(numRefrig) {
	cabinetRefrigs=malloc(numRefrig*sizeof(refrigConfig));
	if(!cabinetRefrigs) { if(cabinetShelfs) free(cabinetShelfs); return(0);}
	}

mutexStatus=semget(IPC_ID,1,IPC_CREAT|0644);
if(mutexStatus==-1) {perror("Creating mutex"); return(0);}
if(semctl(mutexStatus,0,SETVAL,1)==-1) {perror("Initializing mutex"); return(0);}


sharedStatus=shmget(IPC_ID,1,IPC_CREAT|0644);
if(sharedStatus==-1) {perror("Creating 1 byte SHM"); return(0);}
if(shmctl(sharedStatus, IPC_RMID, &b)==-1) {perror("Removing old SHM"); return(0);}
sharedStatus=shmget(IPC_ID,numShelfs*sizeof(shelfStatus),IPC_CREAT|0644);
if(sharedStatus==-1) {perror("Creating new SHM"); return(0);}

cabinetStatus=(shelfStatus *)shmat(sharedStatus,NULL,0);
if(cabinetStatus==(shelfStatus *)-1) {perror("Attaching SHM"); return(0);}

lockMutex();

rewind(f);
gotRefrig=gotShelfs=0;
while(fgets(line,200,f))
	{
        if(!strncmp(line,"S ",2))
                {
		line[strlen(line)-1]=0;

		aux=getNextField(line+2,tmp); if(!aux) break; bb=atoi(tmp); if(!bb) break;
		cabinetShelfs[gotShelfs].shelfHeight=bb;

		aux=getNextField(aux,tmp); if(!aux) break;  bb=atoi(tmp); if(!bb) break;
		cabinetShelfs[gotShelfs].shelfWidth=bb;

		aux=getNextField(aux,tmp); if(!aux) break;  bb=atoi(tmp); if(!bb) break;
		cabinetShelfs[gotShelfs].shelfDepth=bb;

		aux=getNextField(aux,tmp); if(!aux) break;  bb=atoi(tmp); if(!bb) break;
		cabinetShelfs[gotShelfs].shelfGroundClearance=bb;

		aux=getNextField(aux,tmp); if(!aux) break;  bb=atoi(tmp); if(!bb) break;
		cabinetShelfs[gotShelfs].shelfRefrigerationSystem=bb;

		aux=getNextField(aux,cabinetShelfs[gotShelfs].shelfOpenCmd); if(!aux) break;
		aux=getNextField(aux,cabinetShelfs[gotShelfs].shelfCloseCmd); if(!aux) break;
		aux=getNextField(aux,cabinetShelfs[gotShelfs].shelfAccessCmd); if(!aux) break;
		aux=getNextField(aux,cabinetShelfs[gotShelfs].shelfReadStatusCmd); if(!aux) break;
		aux=getNextField(aux,cabinetShelfs[gotShelfs].shelfReadTempereatureCmd); if(!aux) break;


		cabinetStatus[gotShelfs].shelfStatus=0; // CLOSED
		cabinetStatus[gotShelfs].shelfRefrigerationOn=0; // OFF
		cabinetStatus[gotShelfs].shelfTemperatureKelvin=0; // NOT SUPPORTED

		gotShelfs++;
                }
        else
        if(!strncmp(line,"R ",2))
                {
		line[strlen(line)-1]=0;
		aux=getNextField(line+2,cabinetRefrigs[gotRefrig].refrigReadTempereatureCmd); if(!aux) break;
		aux=getNextField(aux,cabinetRefrigs[gotRefrig].refrigOnCmd); if(!aux) break;
		aux=getNextField(aux,cabinetRefrigs[gotRefrig].refrigOffCmd); if(!aux) break;
		aux=getNextField(aux,cabinetRefrigs[gotRefrig].refrigStatusCmd); if(!aux) break;
		gotRefrig++;
                }
	}

unlockMutex();
fclose(f);
if(gotShelfs!=numShelfs || gotRefrig!=numRefrig) { puts("Error parsing physics file"); return(0);}
return(1);
}

int copyStatus2msg(unsigned char *place)
{
int i;
unsigned char *aux=place;
*aux=numShelfs; aux++;
lockMutex();
for(i=0;i<numShelfs; i++)
	{
	*aux=cabinetStatus[i].shelfStatus; aux++;
	*aux=cabinetStatus[i].shelfRefrigerationOn; aux++;
	*aux=cabinetStatus[i].shelfTemperatureKelvin/256; aux++;
	*aux=cabinetStatus[i].shelfTemperatureKelvin%256; aux++;
	}
unlockMutex();
return(aux-place);
}

int copyConfig2msg(unsigned char *place)
{
int i;
unsigned char *aux=place;
*aux=numShelfs; aux++;
for(i=0;i<numShelfs; i++)
	{
	*aux=cabinetShelfs[i].shelfHeight; aux++;
	*aux=cabinetShelfs[i].shelfWidth; aux++;
	*aux=cabinetShelfs[i].shelfDepth; aux++;
	*aux=cabinetShelfs[i].shelfGroundClearance; aux++;
	if(cabinetShelfs[i].shelfRefrigerationSystem>0) *aux=1; else *aux=0;
	aux++;
	}
return(aux-place);
}


void execCommand(unsigned char ord, unsigned char shelf)
{
FILE *p;
char *command;
char output[100];
int pid;

if(shelf>numShelfs)
	{
	puts("Invalid shelf number");
	return;
	}

pid=fork();
if(pid<0) {perror("Fork");return;}
if(pid>0) return; // FATHER RETURNS

// CHILD HAS WORK TO DO

switch(ord) {
	case 0: command=cabinetShelfs[shelf].shelfOpenCmd; break;
	case 1: command=cabinetShelfs[shelf].shelfCloseCmd; break;
	case 2: command=cabinetShelfs[shelf].shelfAccessCmd; break;
	default:
		puts("Unsupported command");
		exit(1);
	}
p=popen(command,"r");
if(!p) { printf("Failed to run command: %s\n",command); exit(1);}
if(ord==2)
	{
	lockMutex(); cabinetStatus[shelf].shelfStatus=2; unlockMutex();
	}
fgets(output,100,p);
pclose(p);
if(!strncmp(output,"OK:",3))
	{
	lockMutex();
	switch(ord) {
		case 0: cabinetStatus[shelf].shelfStatus=1; break;
		case 1: cabinetStatus[shelf].shelfStatus=0;; break;
		case 2: cabinetStatus[shelf].shelfStatus=0; break;
		}
	unlockMutex();
	}

exit(0);
}
